import { Component, OnInit } from "@angular/core";
import { DogsService } from "../../services";
import { Observable } from "../../../../node_modules/rxjs";
import { Dog } from "../../model/dog.model";

@Component({
  selector: "app-dog-list",
  templateUrl: "./dog-list.component.html",
  styleUrls: ["./dog-list.component.css"]
})
export class DogListComponent implements OnInit {
  dogs$: Observable<Dog[]>;
  constructor(private dogService: DogsService) {}

  ngOnInit() {
    this.dogs$ = this.dogService.getDogs();
  }
  getRoute(dog) {
    return ["./edit", dog.id];
  }
  removeItem(dog) {
    this.dogService.removeDog(dog).subscribe(success => {
      this.dogs$ = this.dogService.getDogs();
    });
  }
}
