import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable, pipe } from "rxjs";
import { switchMap, filter, catchError } from "rxjs/operators";

import { Dog } from "../../model/dog.model";
import { Race } from "../../model/race.model";
import { DogsService, RacesService } from "../../services";

@Component({
  selector: "app-dog",
  templateUrl: "./dog.component.html",
  styleUrls: ["./dog.component.css"]
})
export class DogComponent implements OnInit {
  dog$: Observable<Dog>;
  races$: Observable<Race[]>;
  error = "";
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dogService: DogsService,
    private racesService: RacesService
  ) {}

  ngOnInit() {
    this.dog$ = this.route.params.pipe(
      filter(({ dogId }) => !!dogId),
      switchMap(({ dogId }) => this.dogService.getDog(dogId))
    );
    this.races$ = this.racesService.getRaces();
  }

  onSubimit(dog: Dog) {
    this.error = "";
    const stream =
      dog && dog.id
        ? this.dogService.updateDog(dog)
        : this.dogService.createDog(dog);
    stream.subscribe(
      dog => {
        this.router.navigate(["/dogs"]);
      },
      error => {
        this.error = "Somthing goes wrong... ";
      }
    );
  }
}
