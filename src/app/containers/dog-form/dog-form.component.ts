import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  OnChanges,
  SimpleChanges
} from "@angular/core";
import { Dog } from "../../model/dog.model";
import { FormBuilder, Validators, FormControl } from "@angular/forms";

@Component({
  selector: "app-dog-form",
  templateUrl: "./dog-form.component.html",
  styleUrls: ["./dog-form.component.css"]
})
export class DogFormComponent implements OnInit, OnChanges {
  @Input() dog: Dog;
  @Input() races: Dog;
  @Output() submit = new EventEmitter<Dog>();

  form = this.fb.group({
    name: [
      "",
      [
        Validators.required,
        Validators.maxLength(20),
        Validators.minLength(3),
        Validators.pattern(/^[a-z]+$/)
      ]
    ],
    age: ["", [Validators.max(30), Validators.min(0)]],
    race: null
  });

  constructor(private fb: FormBuilder) {}

  ngOnInit() {}

  onSubmit() {
    if (this.form.valid) {
      this.submit.emit({ ...this.form.value });
    }
  }

  ngOnChanges(change: SimpleChanges) {
    if (change.dog && this.dog) {
      this.form.patchValue(this.dog);
    }
  }
  get nameErrors() {
    const nameForm = this.form.get("name") as FormControl;
    if (nameForm.valid || !this.form.touched) {
      return null;
    }
    const { errors } = nameForm;
    if (errors.minlength) {
      return "Name must have minimum 3 characters";
    }
    if (errors.maxlength) {
      return "Name must have maximum 3 characters";
    }
    if (errors.pattern) {
      return "only small characters are allow";
    }
  }
  get ageErrors() {
    const nameForm = this.form.get("age") as FormControl;
    if (nameForm.valid || !this.form.touched) {
      return null;
    }
    const { errors } = nameForm;
    if (errors.max) {
      return "age must be positive numbers";
    }
    if (errors.min) {
      return "max age allowed is 30";
    }
  }
  compareFn(c1: Dog, c2: Dog): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
