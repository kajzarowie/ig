import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MainPageComponent } from "./components/main-page/main-page.component";
import { DogListComponent } from "./components/dog-list/dog-list.component";
import { DogComponent } from "./components/dog/dog.component";

const routes: Routes = [
  {
    path: "",
    component: MainPageComponent
  },
  {
    path: "dogs",
    children: [
      { path: "", component: DogListComponent },
      { path: "new", component: DogComponent },
      { path: "edit/:dogId", component: DogComponent }
    ]
  },
  {
    path: "**",
    redirectTo: ""
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
