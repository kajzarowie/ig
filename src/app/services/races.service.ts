import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Race } from "../model/race.model";

const API_URL = `http://localhost:3000/races`;

@Injectable()
export class RacesService {
  constructor(private http: HttpClient) {}

  getRaces() {
    return this.http.get<Race[]>(API_URL);
  }
}
