import { DogsService } from "./dogs.service";
import { RacesService } from "./races.service";

export const services = [DogsService, RacesService];
export * from "./dogs.service";
export * from "./races.service";
