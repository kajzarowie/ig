import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Dog } from "../model/dog.model";

const API_URL = `http://localhost:3000/dogs`;

@Injectable()
export class DogsService {
  constructor(private http: HttpClient) {}

  getDogs() {
    return this.http.get<Dog[]>(API_URL);
  }
  getDog(id) {
    return this.http.get<Dog>(`${API_URL}/${id}`);
  }
  createDog(dog: Dog) {
    return this.http.post<Dog>(API_URL, dog);
  }

  updateDog(dog: Dog) {
    return this.http.put<Dog>(`${API_URL}/${dog.id}`, dog);
  }

  removeDog(dog: Dog) {
    return this.http.delete<any>(`${API_URL}/${dog.id}`);
  }
}
