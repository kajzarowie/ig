import { Race } from "./race.model";

export interface Dog {
  id: number;
  name: string;
  age: number;
  race: Race;
}
