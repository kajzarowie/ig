import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { DogComponent } from "./components/dog/dog.component";
import { DogListComponent } from "./components/dog-list/dog-list.component";
import { MainPageComponent } from "./components/main-page/main-page.component";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import * as fromServices from "./services";
import { DogFormComponent } from "./containers/dog-form/dog-form.component";
@NgModule({
  declarations: [
    AppComponent,
    DogComponent,
    DogListComponent,
    MainPageComponent,
    DogFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [...fromServices.services],
  bootstrap: [AppComponent]
})
export class AppModule {}
